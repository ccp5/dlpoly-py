#!/usr/bin/env python3
"""
Run DLPoly from commandline using arguments from `dlpoly.cli`.
"""

# from .cli import get_command_args
from .dlpoly import main

main()
