====
msd
====

API
___

.. automodule:: dlpoly.msd
   :members:
   :special-members: __init__
   :undoc-members:
